function distance(first, second) {
    //TODO: implementați funcția
    // TODO: implement the function

    if (Object.prototype.toString.call(first) != '[object Array]' || Object.prototype.toString.call(second) != '[object Array]') {
        throw new Error("InvalidType");
    } else if (!first.length && !second.length) {
        return 0
    } else {
        let a = 0
        const x = [...new Set(first)]
        const y = [...new Set(second)]
        x.forEach(e => {
            y.indexOf(e) === -1 ? a++ : a
        })
        y.forEach(e => {
            x.indexOf(e) === -1 ? a++ : a
        })
        return a
    }
}
module.exports.distance = distance